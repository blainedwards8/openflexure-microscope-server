Developing API Extensions
=========================

.. toctree::
   :maxdepth: 2

   ./extensions/introduction.rst
   ./extensions/structure.rst
   ./extensions/views.rst
   ./extensions/marshaling.rst
   ./extensions/properties.rst
   ./extensions/actions.rst
   ./extensions/tasks_locks.rst
   ./extensions/ev_gui.rst
   ./extensions/lifecycle_hooks.rst