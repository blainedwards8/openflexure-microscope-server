# Bug report

## Summary

(Summarize the bug encountered concisely)

## Configuration

I'm using:

**Camera:** (E.g. Raspberry Pi camera v2)

**Motor controller:** (E.g. Sangaboard, or Arduino Nano)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Relevant logs and/or screenshots

To access your microscope log, either:

* Run `ofm log` on your microscope, and copy/paste the output here

* Go to `http://<your microscope IP>:5000/log`, download the log file, and attach it here using the "Attach a File" button below
  * In most setups, `http://microscope.local:5000/log` should work

## Additional details

(Anything else you think might be relevant to mention)

/label ~bug
/cc @jtc42
